package com.lendenclub.steps;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class LoanStatusCount {
	WebDriver driver;

	public LoanStatusCount(WebDriver driver)
	{
		this.driver=driver;
		driver.manage().window().fullscreen();

		WebDriverWait wait2 = new WebDriverWait(driver, 5);
		WebElement element1 = wait2.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
				"//div[@class='ViewFooter__ToggleIcon-expQZd iECcCZ sc-htpNat friNfU']")));
		element1.click();
		//		try {
		//			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[2]/div/div[2]/div/div[1]/svg/path")).click();
		//		}catch(Exception e) {
		//			driver.findElement(By.xpath("//div[@class='ViewFooter__ToggleIcon-expQZd iECcCZ sc-htpNat friNfU']")).click();
		//		}
		try {
			WebDriverWait wait = new WebDriverWait(driver, 1);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("613f36402")));
		} catch (Exception ex) {
			ex.getMessage();
		}
		try {
			// Date format for screenshot file name
			SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy__hh_mm_ssaa");
			Date date = new Date();
			String fileName = df.format(date);
			File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			// coppy screenshot file into screenshot folder.
			FileUtils.copyFile(file, new File(System.getProperty("user.dir") + "/ImPlusLoanCount/"+fileName+".jpg"));
		} catch(Exception e) { }

	}
}
