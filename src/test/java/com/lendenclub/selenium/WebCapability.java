package com.lendenclub.selenium;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class WebCapability {
static WebDriver driver;
	
	public static WebDriver WebCapability()
	{
		try 
		{
			ChromeOptions opt = new ChromeOptions();
			opt.addArguments("--remote-allow-origins=*");
			WebDriverManager.chromedriver().setup();
			opt.addArguments("no-sandbox");
		    opt.addArguments("headless");
		    opt.addArguments("--remote-allow-origins=*");
		    opt.addArguments("window-size=1920,1180");
			driver = new ChromeDriver(opt);
			
			
			File source = new File("./ImPlusLoanCount");
			try {
			
				FileUtils.cleanDirectory(source);
			
			} catch (IOException e) {
				
				e.printStackTrace();
			}
		
		}
		catch(Exception e)
		{}
		
		return driver;
	}


}
