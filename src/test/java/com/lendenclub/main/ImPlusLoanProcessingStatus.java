package com.lendenclub.main;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.lendenclub.selenium.WebCapability;
import com.lendenclub.steps.LoanStatusCount;
import com.lendenclub.steps.LoginMetabase;

public class ImPlusLoanProcessingStatus extends WebCapability{

	WebDriver driver;

	@BeforeTest
	public void openingFireFox()
	{	   
		driver = WebCapability();
	}
	
	@Test(priority = 1)
	public void  loginPage() throws Exception
	{
	  	new LoginMetabase(driver);
	  	
	}
	
	@Test(priority = 2)
	public void  TestLoanProcessingCount() throws Exception
	{
	  	new LoanStatusCount(driver);
	  	
	}
	
	@AfterTest
	public void CloseBrowser()
	{
		
	 	driver.quit();
	}

}
